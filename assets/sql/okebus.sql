-- Membuat user dan privileges-nya
create user okebus identified by okebus;
grant connect to okebus;
grant all privileges to okebus;
disc;
conn okebus;

-- Membuat tabel admin
create table admin (
	username varchar2(50) primary key,
	password varchar2(20) not null
);
-- insert into admin values ('Admin', 'admin');

-- Membuat tabel terminal
create table terminal(
	id_terminal varchar2(10) not null,
	kota_terminal varchar2(25) not null,
	nama_terminal varchar2(25) not null,
	constraint terminal_pk primary key (id_terminal)
);
-- Insert data terminal
insert into terminal values (NULL, 'Bandung', 'Leuwi Panjang');
insert into terminal values (NULL, 'Jakarta', 'Lebak Bulus');

-- Membuat tabel bus
create table bus(
	id_bus varchar2(10) not null,
	jml_kursi number not null,
	kelas varchar2(10) not null,
	harga number not null,
	jam_berangkat varchar2(5) not null,
	jam_datang varchar2(5) not null,
	id_terminal_asal varchar2(5) not null,
	id_terminal_tujuan varchar2(5) not null,
	constraint bus_pk primary key (id_bus),
	constraint terminal_asal_fk foreign key (id_terminal_asal) references terminal (id_terminal) on delete cascade,
	constraint terminal_tujuan_fk foreign key (id_terminal_tujuan) references terminal (id_terminal) on delete cascade
);

-- Membuat tabel kursi
create table kursi(
	id_kr varchar2(10) not null,
	id_bus varchar2(10) not null,
	status number(1) default 0,
	constraint kursi_pk primary key (id_kr),
	constraint bus_fk foreign key (id_bus) references bus (id_bus) on delete cascade
);

-- Membuat tabel penumpang
create table penumpang(
	id_pn varchar2(25) not null,
	nama_pn varchar2(50) not null,
	constraint penumpang_pk primary key (id_pn)
);

-- Membuat tabel pemesan
create table pemesan(
	id_pm varchar2(25) not null,
	nama_pm varchar2(50) not null,
	email_pm varchar2(50) not null,
	telp_pm varchar2(15) not null,
	alamat_pm varchar2(50) not null,
	constraint pemesan_pk primary key (id_pm)
);

-- Membuat tabel jadwal
create table jadwal(
	id_jadwal varchar2(10) not null,
	tgl date not null,
	id_bus varchar2(10) not null,
	jml_kursi number not null,
	constraint jadwal_pk primary key (id_jadwal),
	constraint bus_fk2 foreign key (id_bus) references bus (id_bus) on delete cascade
);

-- Membuat tabel tiket
create table tiket(
	id_tk varchar2(10) not null,
	id_kr varchar2(10) not null,
	id_pn varchar2(25) not null,
	id_pm varchar2(25) not null,
	id_jadwal varchar2(10),
	tgl date not null,
	constraint tiket_pk primary key (id_tk),
	constraint kr_fk foreign key (id_kr) references kursi (id_kr) on delete cascade,
	constraint pn_fk foreign key (id_pn) references penumpang (id_pn) on delete cascade,
	constraint pm_fk foreign key (id_pm) references pemesan (id_pm) on delete cascade,
	constraint jd_fk foreign key (id_jadwal) references jadwal (id_jadwal) on delete cascade
);

-- Membuat tabel tempat_pembayaran
create table tempat_pembayaran(
	id_tm varchar2(10) not null,
	nama_tm varchar2(25) not null,
	constraint tm_pk primary key (id_tm)
);
-- insert into tempat_pembayaran values ('TM0001', 'ATM');

-- Membuat table transaksi
create table transaksi(
	id_tr varchar2(20) not null,
	id_tk varchar2(10) not null,
	id_tm varchar2(10),
	status number(1) not null,
	constraint tk_fk foreign key (id_tk) references tiket (id_tk) on delete cascade,
	constraint tm_fk foreign key (id_tm) references tempat_pembayaran (id_tm) on delete cascade
);

-- Membuat sequence untuk tabel(s)
create sequence seq_id_bus increment by 1;
create sequence seq_id_kr increment by 1;
create sequence seq_id_jd increment by 1;
create sequence seq_id_tk increment by 1;
create sequence seq_id_tr increment by 1;
create sequence seq_id_pm increment by 1;
create sequence seq_id_pn increment by 1;

-- Trigger generate id_penumpang
create or replace trigger gk_pn
before insert on penumpang
for each row
declare
	v_char varchar2(10);
	v_id_pn number;
begin
	if :new.id_pn is null then
		select seq_id_pn.nextval into v_id_pn from dual;
		v_char := '00';
		if(v_id_pn >= 10 and v_id_pn < 100) then
			v_char := '0';
		elsif(v_id_pn >= 100) then
			v_char := null;
		end if;
		select 'PN'||v_char||to_char(v_id_pn) into :new.id_pn from dual;
	end if;
end;
/

-- Trigger generate id_pemesan
create or replace trigger gk_pm
before insert on pemesan
for each row
declare
	v_char varchar2(10);
	v_id_pm number;
begin
	if :new.id_pm is null then
		select seq_id_pm.nextval into v_id_pm from dual;
		v_char := '00';
		if(v_id_pm >= 10 and v_id_pm < 100) then
			v_char := '0';
		elsif(v_id_pm >= 100) then
			v_char := null;
		end if;
		select 'PM'||v_char||to_char(v_id_pm) into :new.id_pm from dual;
	end if;
end;
/

-- Trigger generate id_terminal
create or replace trigger gk_tr
before insert on terminal
for each row
declare
	v_char varchar2(10);
	v_id_tr number;
begin
	if :new.id_terminal is null then
		select seq_id_tr.nextval into v_id_tr from dual;
		v_char := '00';
		if(v_id_tr >= 10 and v_id_tr < 100) then
			v_char := '0';
		elsif(v_id_tr >= 100) then
			v_char := null;
		end if;
		select 'TR'||v_char||to_char(v_id_tr) into :new.id_terminal from dual;
	end if;
end;
/

-- Trigger generate id_bus
create or replace trigger gk_bus
before insert on bus
for each row
declare
	v_char varchar2(10);
	v_id_bus number;
begin
	if :new.id_bus is null then
		select seq_id_bus.nextval into v_id_bus from dual;
		v_char := '00';
		if(v_id_bus >= 10 and v_id_bus < 100) then
			v_char := '0';
		elsif(v_id_bus >= 100) then
			v_char := null;
		end if;
		select 'BS'||v_char||to_char(v_id_bus) into :new.id_bus from dual;
	end if;
end;
/

-- Trigger generate kursi secara otomatis sesuai jml_kursi di bus, id_kursi digenerate
create or replace trigger gk_kr
after insert on bus
for each row
declare
	v_counter number;
	v_char varchar2(10);
	v_id_kr number;
begin
	v_counter := 0;
	loop
		select seq_id_kr.nextval into v_id_kr from dual;
		v_char := '00';
		if(v_id_kr >= 10 and v_id_kr < 100) then
			v_char := '0';
		elsif(v_id_kr >= 100) then
			v_char := null;
		end if;
		insert into kursi values ('KR'||v_char||to_char(v_id_kr), :new.id_bus, 0);
	v_counter := v_counter + 1;
	if(v_counter = :new.jml_kursi) then
	exit;
	end if;
	end loop;
end;
/

-- Trigger generate jadwal saat membeli tiket, jml_kursi di jadwal berkurang 1, data kursi di tabel kursi status jadi 1 karena sudah dipesan
create or replace trigger gk_jd
before insert on tiket
for each row
declare
	v_char varchar2(10);
	v_char2 varchar2(10);
	v_id_tk number;
	v_id_jadwal number;
	v_id_bus varchar2(10);
	v_jml_jadwal number;
	v_jml_kursi number;
begin
	if :new.id_tk is null then
		select seq_id_tk.nextval into v_id_tk from dual;
		v_char := '00';
		if(v_id_tk >= 10 and v_id_tk < 100) then
			v_char := '0';
		elsif(v_id_tk >= 100) then
			v_char := null;
		end if;
		select 'TK'||v_char||to_char(v_id_tk) into :new.id_tk from dual;
	end if;
	
	select id_bus into v_id_bus from kursi where id_kr = :new.id_kr;
	select count(*) into v_jml_jadwal from jadwal where jadwal.tgl = :new.tgl and jadwal.id_bus = v_id_bus;
	select jml_kursi into v_jml_kursi from bus, kursi where bus.id_bus = kursi.id_bus and kursi.id_kr = :new.id_kr;

	if(v_jml_jadwal = 0) then
		if :new.id_jadwal is null then

			select seq_id_jd.nextval into v_id_jadwal from dual;
			v_char2 := '00';
			if(v_id_jadwal >= 10 and v_id_jadwal < 100) then
				v_char2 := '0';
			elsif(v_id_jadwal >= 100) then
				v_char2 := null;
			end if;
	
			select 'JD'||v_char2||to_char(v_id_jadwal) into :new.id_jadwal from dual;
			insert into jadwal values ('JD'||v_char2||to_char(v_id_jadwal), :new.tgl, v_id_bus, v_jml_kursi-1);
			update kursi set status = 1 where id_kr = :new.id_kr;
		end if;
	elsif(v_jml_jadwal = 1) then
		select id_jadwal into :new.id_jadwal from jadwal where jadwal.id_bus = v_id_bus;
		update jadwal set jml_kursi = jml_kursi - 1 where jadwal.id_jadwal = :new.id_jadwal;
		update kursi set status = 1 where id_kr = :new.id_kr;
	end if;
end;
/

-- Trigger mengubah jml_kursi+1 dan status kursi jadi tersedia(0) saat tiket batal dibeli(data dihapus)
create or replace trigger dl_tk
after delete on tiket
for each row
declare
	v_id_kr varchar2(10);
begin
	select :old.id_kr into v_id_kr from dual;
	update jadwal set jml_kursi = jml_kursi + 1 where jadwal.id_jadwal = :old.id_jadwal;
	update kursi set status = 0 where id_kr = v_id_kr;
end;
/