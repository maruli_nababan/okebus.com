<?php

// Connection to database function
function db_connect($username, $password, $database){
	$connect = oci_connect($username, $password, $database);
	if(!$connect){
		$err = oci_error();
		echo "Koneksi Oracle Gagal! " . $err['text'];
	}
	return $connect;
}

// Select Query function
function db_query($connect, $query){
	$result = oci_parse($connect, $query);
	oci_execute($result);
	$rows = oci_fetch_all($result, $res);
	if($rows > 0){
		oci_execute($result);
		$row = [];
		while(($data = oci_fetch_object($result)) != false){
			$row[] = $data;
		}
		return $row;
	}else{
		$err = oci_error();
		//echo "Erorr : " . $err['text'];
		return false;
	}
}

// Insert Query function
function db_insert($connect, $table, $POST){
	$query = "insert into $table (";
	foreach($POST as $name => $value){
		$query .= "$name";
		$query .= $value != end($POST) ? ", " : "";
	}
	$query .= ") values (";
	foreach($POST as $name => $value){
		$query .= "'$value'";
		$query .= $value != end($POST) ? ", " : "";
	}
	$query .= ")";
	$result = oci_parse($connect, $query);
	oci_execute($result);
	if($result){
		return true;
	}else{
		$err = oci_error();
		echo $query."<br/>Erorr : " . $err['text'];
    }
}

function db_custom_insert($connect, $query){
	$result = oci_parse($connect, $query);
	oci_execute($result);
	if($result){
		return true;
		//header('Location: index.php');
	}else{
		$err = oci_error();
		echo $query."<br/>Erorr : " . $err['text'];
	}
}


// Update Query function
function db_update($connect, $table, $POST, $par, $id){
	$query = "update $table SET ";
	foreach($POST as $name => $value){
		$query .= "$name = '$value'";
		$query .= $value != end($POST) ? ", " : " ";
    }
	$query .= "where $par = $id";
	$result = oci_parse($connect, $query);
	oci_execute($result);
	if($result){
		return true;
	}else{
		$err = oci_error();
		echo $query."<br/>Erorr : " . $err['text'];
	}
}

// Delete Query function
function db_delete($connect, $table, $par, $id){
	$query = "delete from $table where $par = $id";
	$result = oci_parse($connect, $query);
	oci_execute($result);
	if($result){
		return true;
	}else{
		$err = oci_error();
		echo $query."<br/>Erorr : " . $err['text'];
	}
}

?>