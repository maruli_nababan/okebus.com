<?php
	// Import Needed Class
	require 'db_function.php';
	require 'db_config.php';
	
	// Create connection variable
	$db_connect = oci_connect(
		$db_config['db_username'],
		$db_config['db_password'],
		$db_config['db_database']
	);
	
?>
