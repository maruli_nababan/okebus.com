create user okebus identified by okebus;
grant connect to okebus;
grant all privileges to okebus;
disc;
conn okebus;

create table admin (
	username varchar2(50) primary key,
	password varchar2(20) not null
);

insert into admin values ('Admin', 'admin');

create table terminal(
	id_terminal varchar2(10) not null,
	kota_terminal varchar2(25) not null,
	constraint stasiun_pk primary key (id_st)
);

create table bus(
	id_bus varchar2(10) not null,
	jml_kursi number not null,
	jam_berangkat varchar2(5) not null,
	jam_datang varchar2(5) not null,
	id_terminal_asal varchar2(5) not null,
	id_terminal_tujuan varchar2(5) not null,
	kelas varchar2(10) not null,
	harga number not null,
	constraint kereta_pk primary key (id_ka),
	constraint terminal_asal_fk foreign key (id_terminal_asal) references terminal (id_terminal) on delete cascade,
	constraint terminal_tujuan_fk foreign key (id_terminal_tujuan) references terminal (id_terminal) on delete cascade
);

-- insert into kereta (nama_ka, jml_gerbong, jam_berangkat, jam_datang, id_st_asal, id_st_tujuan) values ('Argo Parahyangan', 3, '08.00', '12.00', 'ST003', 'ST004');
-- insert into kereta (nama_ka, jml_gerbong, jam_berangkat, jam_datang, id_st_asal, id_st_tujuan) values ('Ciremai', 3, '06.00', '08.00', 'ST004', 'ST003');

create table kursi(
	id_kr varchar2(10) not null,
	id_bus varchar2(10) not null,
	constraint kursi_pk primary key (id_kr),
	constraint bus_fk foreign key (id_bus) references bus (id_bus) on delete cascade
);

create table penumpang(
	id_pn varchar2(25) not null,
	nama_pn varchar2(50) not null,
	constraint penumpang_pk primary key (id_pn)
);

create table pemesan(
	id_pm varchar2(25) not null,
	nama_pm varchar2(50) not null,
	email_pm varchar2(50) not null,
	telp_pm varchar2(15) not null,
	alamat_pm varchar2(50) not null,
	constraint pemesan_pk primary key (id_pm)
);

create table jadwal(
	id_jadwal varchar2(10) not null,
	tgl date not null,
	id_bus varchar2(10) not null,
	jml_kursi number not null,
	constraint jadwal_pk primary key (id_jadwal) 
);

create table tiket(
	id_tk varchar2(10) not null,
	id_kr varchar2(10) not null,
	id_pn varchar2(25) not null,
	id_pm varchar2(25) not null,
	id_jadwal varchar2(10),
	tgl date not null,
	constraint tiket_pk primary key (id_tk),
	constraint kr_fk foreign key (id_kr) references kursi (id_kr) on delete cascade,
	constraint pn_fk foreign key (id_pn) references penumpang (id_pn) on delete cascade,
	constraint pm_fk foreign key (id_pm) references pemesan (id_pm) on delete cascade,
	constraint jd_fk foreign key (id_jadwal) references jadwal (id_jadwal) on delete cascade
);

create table tempat_pembayaran(
	id_tm varchar2(10) not null,
	nama_tm varchar2(25) not null,
	constraint tm_pk primary key (id_tm)
);

insert into tempat_pembayaran values ('TM0001', 'ATM');

create table transaksi(
	id_tr varchar2(20) not null,
	id_tk varchar2(10) not null,
	id_tm varchar2(10),
	status number(1) not null,
	constraint tk_fk foreign key (id_tk) references tiket (id_tk) on delete cascade,
	constraint tm_fk foreign key (id_tm) references tempat_pembayaran (id_tm) on delete cascade
);

create sequence seq_id_st increment by 1;
create sequence seq_id_ka increment by 1;
create sequence seq_id_kr increment by 1;
create sequence seq_id_gr increment by 1;
create sequence seq_id_jd increment by 1;
create sequence seq_id_tk increment by 1;
create sequence seq_id_pm increment by 1;

create or replace trigger gk_pm
before insert on pemesan
for each row
declare
	v_char varchar2(10);
	v_id_pm number;
begin
	if :new.id_pm is null then
		select seq_id_pm.nextval into v_id_pm from dual;
		v_char := '00';
		if(v_id_pm >= 10 and v_id_pm < 100) then
			v_char := '0';
		elsif(v_id_pm >= 100) then
			v_char := null;
		end if;
		select 'PM'||v_char||to_char(v_id_pm) into :new.id_pm from dual;
	end if;
end;
/

create or replace trigger gk_st
before insert on stasiun
for each row
declare
	v_char varchar2(10);
	v_id_st number;
begin
	if :new.id_st is null then
		select seq_id_st.nextval into v_id_st from dual;
		v_char := '00';
		if(v_id_st >= 10 and v_id_st < 100) then
			v_char := '0';
		elsif(v_id_st >= 100) then
			v_char := null;
		end if;
		select 'ST'||v_char||to_char(v_id_st) into :new.id_st from dual;
	end if;
end;
/

-- insert into stasiun (kota_st, nama_st) values ('Jakarta', 'Gambir');
-- insert into stasiun (kota_st, nama_st) values ('Bandung', 'Parahyangan');

create or replace trigger gk_ka
before insert on kereta
for each row
declare
	v_char varchar2(10);
	v_id_ka number;
begin
	if :new.id_ka is null then
		select seq_id_ka.nextval into v_id_ka from dual;
		v_char := '00';
		if(v_id_ka >= 10 and v_id_ka < 100) then
			v_char := '0';
		elsif(v_id_ka >= 100) then
			v_char := null;
		end if;
		select 'KA'||v_char||to_char(v_id_ka) into :new.id_ka from dual;
	end if;
end;
/

create or replace trigger gk_gr
after insert on kereta
for each row
declare
	v_counter number;
	v_kelas varchar2(10);
	v_harga number;
	v_id_gr number;
	v_char varchar2(10);
	v_char2 varchar2(10);
begin
	v_counter := 1;
	loop
		select seq_id_gr.nextval into v_id_gr from dual;
		if(v_counter = 1) then
			v_kelas := 'Bisnis';
			v_harga := 50000;
			v_char2 := 'BI';
		elsif(v_counter = 2) then
			v_kelas := 'Ekonomi';
			v_harga := 25000;
			v_char2 := 'EK';
		else
			v_kelas := 'Eksekutif';
			v_harga := 75000;
			v_char2 := 'ES';
		end if;
		
		v_char := '00';
		if(v_id_gr >= 10 and v_id_gr < 100) then
			v_char := '0';
		elsif(v_id_gr >= 100) then
			v_char := null;
		end if;
		
		insert into gerbong values (v_char2||v_char||to_char(v_id_gr), :new.id_ka, 20, v_kelas, v_harga);
	v_counter := v_counter + 1;
	if(v_counter > 3) then
	exit;
	end if;

	end loop;
end;
/

create or replace trigger gk_kr
after insert on gerbong
for each row
declare
	v_counter number;
	v_char varchar2(10);
	v_id_kr number;
begin
	v_counter := 0;
	loop
		select seq_id_kr.nextval into v_id_kr from dual;
		v_char := '00';
		if(v_id_kr >= 10 and v_id_kr < 100) then
			v_char := '0';
		elsif(v_id_kr >= 100) then
			v_char := null;
		end if;
		insert into kursi values ('KR'||v_char||to_char(v_id_kr), :new.id_gr);
	v_counter := v_counter + 1;
	if(v_counter = 20) then
	exit;
	end if;
	end loop;
end;
/

	create or replace trigger gk_jd
	before insert on tiket
	for each row
	declare
		v_char varchar2(10);
		v_char2 varchar2(10);
		v_id_tk number;
		v_id_jadwal number;
		v_id_gr varchar2(10);
		v_jml_jadwal number;
		v_jml_kursi number;
	begin
		if :new.id_tk is null then
			select seq_id_tk.nextval into v_id_tk from dual;
			v_char := '00';
			if(v_id_tk >= 10 and v_id_tk < 100) then
				v_char := '0';
			elsif(v_id_tk >= 100) then
				v_char := null;
			end if;
			select 'TK'||v_char||to_char(v_id_tk) into :new.id_tk from dual;
		end if;
		
		select id_gr into v_id_gr from kursi where id_kr = :new.id_kr;
		select count(*) into v_jml_jadwal from jadwal where jadwal.tgl = :new.tgl and jadwal.id_gr = v_id_gr;
		select jml_kursi into v_jml_kursi from gerbong, kursi where gerbong.id_gr = kursi.id_gr and kursi.id_kr = :new.id_kr;

		if(v_jml_jadwal = 0) then
			if :new.id_jadwal is null then

				select seq_id_jd.nextval into v_id_jadwal from dual;
				v_char2 := '00';
				if(v_id_jadwal >= 10 and v_id_jadwal < 100) then
					v_char2 := '0';
				elsif(v_id_jadwal >= 100) then
					v_char2 := null;
				end if;
		
				select 'JD'||v_char2||to_char(v_id_jadwal) into :new.id_jadwal from dual;
				insert into jadwal values ('JD'||v_char2||to_char(v_id_jadwal), :new.tgl, v_id_gr, v_jml_kursi-1);
			end if;
		elsif(v_jml_jadwal = 1) then
			select id_jadwal into :new.id_jadwal from jadwal where jadwal.id_gr = v_id_gr;
			update jadwal set jml_kursi = jml_kursi - 1 where jadwal.id_jadwal = :new.id_jadwal;
		end if;
	end;
	/
	
create or replace trigger dl_tk
after delete on tiket
for each row
declare
begin
	update jadwal set jml_kursi = jml_kursi - 1 where jadwal.id_jadwal = :old.id_jadwal;
end;
/


-- insert into penumpang values ('1404416', 'Maruli T N');
-- insert into pemesan values ('1404416', 'Maruli T N', 'marulinababan@ymail.com', '08968383683', 'Bandung');
--insert into tiket (id_kr, id_pn, id_pm, tgl) values ('KR168', '1404416', '1404416', '20-DEC-2015');


-- select a.*, (select jml_kursi from v_status where id_gr = a.id_gr and tgl = '21-DEC-2015') as jml_kursi from v_jadwal a;

create or replace view v_kereta as select a.id_ka, c.id_gr, a.nama_ka, b.id_st as id_st_asal, b.nama_st as nama_st_asal,
	(select id_st from stasiun where id_st = a.id_st_tujuan) as id_st_tujuan,
	(select nama_st from stasiun where id_st = a.id_st_tujuan) as nama_st_tujuan,
	a.jam_berangkat, a.jam_datang, a.jml_gerbong, c.harga, c.kelas
	from kereta a, stasiun b, gerbong c where a.id_st_asal = b.id_st and a.id_ka = c.id_ka;

create or replace view v_kereta2 as select a.id_ka, a.nama_ka, b.nama_st as st_asal, (select nama_st from stasiun where id_st = a.id_st_tujuan) as st_tujuan, a.jam_berangkat, a.jam_datang, a.jml_gerbong from kereta a join stasiun b on a.id_st_asal = b.id_st;

create or replace view v_jadwal as select a.id_ka,a.nama_ka, b.id_gr, b.jml_kursi, a.id_st_asal, a.id_st_tujuan, a.jam_berangkat, a.jam_datang, b.harga, b.kelas from kereta a, gerbong b, tiket c, kursi d where a.id_ka = b.id_ka and b.id_gr = d.id_gr and d.id_kr = c.id_kr;
create or replace view v_status as select a.id_jadwal, a.tgl, a.jml_kursi, d.id_gr, b.id_kr from jadwal a, tiket b, kursi c, gerbong d where a.id_jadwal = b.id_jadwal and b.id_kr = c.id_kr and c.id_gr = d.id_gr;

-- select a.id_jadwal, count(c.id_tk), a.tgl, a.jml_kursi, b.id_gr, b.jml_kursi from jadwal a, tiket c, kursi d, gerbong b
-- where a.id_jadwal = c.id_jadwal and c.id_kr = d.id_kr and d.id_gr = b.id_gr group by a.id_jadwal;


create or replace procedure fn_transaksi(v_id_tr in varchar2, v_id_tk in varchar2) as
begin
	insert into transaksi values(v_id_tr, v_id_tk, 'TM0001', 0);
end;
/

create or replace procedure prc_transaksi (v_id_tk in varchar2, v_id_tr in varchar2, v_jml_tiket in number, c_hasil out SYS_REFCURSOR) as
begin
	--exec fn_transaksi(v_id_tr, v_id_tk);
	open c_hasil for select v_id_tk as v_jml_tiket, f.id_ka, f.nama_ka, id_tk, a.id_pm, a.nama_pm, a.email_pm, a.telp_pm, a.alamat_pm, b.id_pn, b.nama_pn, c.tgl, c.id_kr, d.kelas, d.harga, f.jam_berangkat, f.jam_datang, g.kota_st as kota_st_asal, g.nama_st as nama_st_asal, (select kota_st from stasiun where id_st = f.id_st_tujuan) as kota_st_tujuan, (select nama_st from stasiun where id_st = f.id_st_tujuan) as nama_st_tujuan from pemesan a, penumpang b, tiket c, gerbong d, kursi e, kereta f, stasiun g where c.id_tk = v_id_tk and a.id_pm = c.id_pm and b.id_pn = c.id_pn and e.id_kr = c.id_kr and e.id_gr = d.id_gr and d.id_ka = f.id_ka and f.id_st_asal = g.id_st;
end prc_transaksi;
/


-- select v_id_tk, a.id_pm, a.nama_pm, a.email_pm, a.telp_pm, a.alamat_pm, b.id_pn, b.nama_pn, c.tgl, c.id_kr, d.kelas, d.harga, g.kota_st, g.nama_st from pemesan a, penumpang b, tiket c, gerbong d, kursi e, kereta f, stasiun g where c.id_tk = v_id_tk and a.id_pm = c.id_pm and b.id_pn = c.id_pn and e.id_kr = c.id_kr and e.id_gr = d.id_gr and d.id_ka = f.id_ka and f.id_st = g.id_st;

select a.*, (select max(jml_kursi) from v_status where id_gr = a.id_gr and tgl = '27-Dec-2015' group by id_gr) as sisa_kursi, (select max(id_kr) from v_status where id_gr = a.id_gr and tgl = '27-Dec-2015' group by id_jadwal) as id_kursi from v_jadwal a WHERE a.id_st_asal = 'ST001' and a.id_st_tujuan = 'ST021' and a.id_ka = 'KA021';