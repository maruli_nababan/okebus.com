<?php
	include "config/url/url.php";
	include "header.php";
	include "config/db/db_connect.php";
?>
<?php if(!isset($_GET['page']) || $_GET['page'] == "info") { ?>
<div class="container">
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<div id="carousel1" class="carousel slide">
				<ol class="carousel-indicators">
					<li data-target="#carousel1" data-slide-to="0" class="active"> </li>
					<li data-target="#carousel1" data-slide-to="1" class=""> </li>
					<li data-target="#carousel1" data-slide-to="2" class=""> </li>
				</ol>
				<div class="carousel-inner">
					<div class="item"> <img class="img-responsive" src="<?php echo $url; ?>assets/gambar/sidebar.gif" alt="thumb">
					</div>
					<div class="item active"> <img class="img-responsive" src="<?php echo $url; ?>assets/gambar/sidebar.gif" alt="thumb">
					</div>
					<div class="item"> <img class="img-responsive" src="<?php echo $url; ?>assets/gambar/sidebar.gif" alt="thumb">
					</div>
				</div>
				<a class="left carousel-control" href="#carousel1" data-slide="prev"><span class="icon-prev"></span></a> <a class="right carousel-control" href="#carousel1" data-slide="next"><span class="icon-next"></span></a></div>
			</div>
		</div>
		<hr>
	</div>
	<h2>Pemesanan Tiket</h2>
	<form method="post" action="index.php?page=tampil">
		<label for="tgl">Tanggal</label>
		<p>
			<select name="tgl" id="tgl">
				<?php
				$date = date_create(date('Y-m-d'));
				for ($i=0; $i < 90; $i++) { 
					$date -> modify(('+1 days'));
					echo "<option value=\"" .$date -> format('Y-m-d') ."\" >" .$date -> format('D, d-m-Y') ."</option>";
				}
				?>
			</select>
		</p>

		<label for="id_terminal_asal">Terminal Asal</label>
		<p>
			<select name="id_terminal_asal" id="id_terminal_asal">
				<option value="">Terminal Asal</option>
				<?php
				$q = "SELECT * FROM terminal";
				$data = db_query($db_connect, $q);
				foreach ($data as $row) {
					echo "<option value=" .$row -> ID_TERMINAL .">" .$row -> NAMA_TERMINAL ." (" .$row -> KOTA_TERMINAL .")</option>";
				}
				?>
			</select>
		</p>

		<label for="id_terminal_tujuan">Terminal Tujuan</label>
		<p>
			<select name="id_terminal_tujuan" id="id_terminal_tujuan">
				<option value="">Terminal Tujuan</option>
				<?php
				$q = "SELECT * FROM terminal";
				$data = db_query($db_connect, $q);
				foreach ($data as $row) {
					echo "<option value=" .$row -> ID_TERMINAL .">" .$row -> NAMA_TERMINAL ." (" .$row -> KOTA_TERMINAL .")</option>";
				}
				?>
			</select>
		</p>

		<label for="jml_penumpang">Jumlah Penumpang</label>
		<p><input type="text" name="jml_penumpang" id="jml_penumpang"></p>

		<p><input type="submit" value="Tampilkan"></p>
	</form>
	<?php }else if($_GET['page'] == "tampil"){
		$asal          = $_POST['id_terminal_asal'];
		$tujuan        = $_POST['id_terminal_tujuan'];
		$tanggal       = $_POST['tgl'];
		$tanggal_      = date("D, d M Y", strtotime($tanggal));
		$jml_penumpang = $_POST['jml_penumpang'];
		$q_terminal    = "SELECT * FROM TERMINAL WHERE ID_TERMINAL = '$asal' or ID_TERMINAL = '$tujuan'";
		$d_terminal    = db_query($db_connect, $q_terminal);
		echo "<p>";
		$i = 0;
		foreach($d_terminal as $row){
			echo "<b>".$row->KOTA_TERMINAL." (".$row->NAMA_TERMINAL.")"."</b>";
			echo $i == 0 ? " - " : "";
			$i++;
		}
		echo "</p>";
	?>
	<p>Jumlah penumpang : <?php echo $jml_penumpang; ?></p>
	<p>Tanggal : <?php echo $tanggal_; ?></p><hr>
	<h3>Pilih Bus</h3>
	<?php
		$q_bus = "SELECT * FROM BUS WHERE ID_TERMINAL_ASAL = '$asal' and ID_TERMINAL_TUJUAN = '$tujuan'";
		$d_bus = db_query($db_connect, $q_bus);
		foreach($d_bus as $row){
	?>
	<table  border="0" align="center" width="87%" height="170" >
	
	
	<tr>
	<td width="18%" rowspan="6"><a href="#"><span aria-hidden="true"></span><img src="<?php echo $url; ?>assets/gambar/pilihbus1.gif"></a>
	<td bgcolor="silver"><a href="#"><span aria-hidden="true"></span> Asal </a>
	<td colspan="11" align="center"><a href="#"><span aria-hidden="true"></span><img src="<?php echo $url; ?>assets/gambar/1.png"> </a>
	
	</tr>
	<tr>
	<td bgcolor="lightgrey" width="18%"><a href="#"><span aria-hidden="true">default</span></a>
	<td rowspan="5" colspan="2" width="3%" align="center"><a href="#"><span aria-hidden="true"></span><img src="<?php echo $url; ?>assets/gambar/24.png"></a>
	</tr>
	<tr>
	<td bgcolor="silver" width="18%" rowspan="2"><a href="#"><span aria-hidden="true"></span></a>
	</tr>
	<tr>
	<td width="18%"><a href="#"><span aria-hidden="true"></span></a>
	</tr>
	<tr>
	<td bgcolor="lightgrey" width="18%"><a href="#"><span aria-hidden="true"></span>Asal</a>
	</tr>
	<tr>
	<td bgcolor="silver" width="18%"><a href="#"><span aria-hidden="true"></span>default</a>
	</tr>
</table>
<br>
	<?php		
		}
	?>
	<?php } ?>

<?php
	include "footer.php";
?>